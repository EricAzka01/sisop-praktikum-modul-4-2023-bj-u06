#include <stdlib.h>
#include <stdio.h>


int main() {

	//1A
	system("kaggle datasets download -d bryanb/fifa-player-stats-database");
	system("unzip fifa-player-stats-database.zip");

	//1B
	system("sort -t',' -k8nr FIFA23_official_data.csv | awk -F',' 'BEGIN { rank = 1; printf \"%-5s %-20s %-10s %-30s %-20s %-10s %-40s\\n\", \"Rank\", \"Name\", \"Age\", \"Club\", \"Nationality\", \"Potential\", \"Photo\" } FNR>1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { printf \"%-5s %-20s %-10s %-30s %-20s %-10s %-40s\\n\", rank, $2, $3, $9, $5, $8, $4; rank++ }'");


    return 0;
}


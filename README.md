# sisop-praktikum-modul-4-2023-bj-u06

Group Members:

Eric Azka Nugroho [5025211064]

Kirana Alivia Enrico [5025211190]

Talitha Hayyinas Sahala [5025211263]

# Question 1

Kota Manchester sedang dilanda berita bahwa kota ini mau juara (yang kota ‘loh ya, bukan yang satunya). Tagline #YBBA (#YangBiruBiruAja #anjaykelaspepnihbossenggoldong 💪🤖🤙🔵⚪) sudah mewabah di seluruh dunia. Semua warga pun sudah menyiapkan pesta besar-besaran. 
Seorang pelatih sepak bola handal bernama Peb merupakan pelatih klub Manchester Blue, sedang berjuang memenangkan Treble Winner. Untuk meraihnya, ia perlu melakukan pembelian pemain dengan ideal. Agar sukses, ia memahami setiap detail data performa pemain sepak bola seluruh dunia yang meliputi statistik pemain, umur, tinggi dan berat badan, potensi, klub dan negaranya, serta banyak data lainnya. Namun, tantangan tersendiri muncul ketika mengelola dan mengakses data berukuran besar ini.
Kesulitan Peb tersebut mencapai telinga kalian, seorang mahasiswa Teknik Informatika yang ahli dalam pengolahan data. Mengetahui tantangan Peb, kalian diminta untuk membantu menyelesaikan masalahnya melalui beberapa langkah berikut.


- Langkah pertama, adalah memperoleh data yang akan digunakan. Kalian membuat file bernama storage.c. Oleh karena itu, download dataset tentang pemain sepak bola dari Kaggle. Dataset ini berisi informasi tentang pemain sepak bola di seluruh dunia, termasuk Manchester Blue. Kalian tahu bahwa dataset ini akan sangat berguna bagi Peb. Gunakan command ini untuk men-download dataset.

```
kaggle datasets download -d bryanb/fifa-player-stats-database
```
Setelah berhasil men-download dalam format .zip, langkah selanjutnya adalah mengekstrak file tersebut.  Kalian melakukannya di dalam file storage.c untuk semua pengerjaannya. 

- Selanjutnya, Peb meminta kalian untuk melakukan analisis awal pada data tersebut dan mencari pemain berpotensi tinggi untuk direkrut. Oleh karena itu, kalian perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Informasi yang dicetak mencakup nama pemain, klub tempat mereka bermain, umur, potensi, URL foto mereka, dan data lainnya. kalian melakukannya di dalam file storage.c untuk analisis ini.

- Peb menyadari bahwa sistem kalian sangat berguna dan ingin sistem ini bisa diakses oleh asisten pelatih lainnya. Oleh karena itu, kalian perlu menjadikan sistem yang dibuat ke sebuah Docker Container agar mudah di-distribute dan dijalankan di lingkungan lain tanpa perlu setup environment dari awal. Buatlah Dockerfile yang berisi semua langkah yang diperlukan untuk setup environment dan menentukan bagaimana aplikasi harus dijalankan.
Setelah Dockerfile berhasil dibuat, langkah selanjutnya adalah membuat Docker Image. Gunakan Docker CLI untuk mem-build image dari Dockerfile kalian. Setelah berhasil membuat image, verifikasi bahwa image tersebut berfungsi seperti yang diharapkan dengan menjalankan Docker Container dan memeriksa output-nya.

- Setelah sukses membuat sistem berbasis Docker, Peb merasa bahwa sistem ini tidak hanya berguna untuk dirinya sendiri, tetapi juga akan akan membantu para scouting-nya yang terpencar di seluruh dunia dalam merekrut pemain berpotensi tinggi. Namun, satu tantangan muncul, yaitu bagaimana caranya para scout dapat mengakses dan menggunakan sistem yang telah diciptakan?
Merasa terpanggil untuk membantu Peb lebih jauh, kalian memutuskan untuk mem-publish Docker Image sistem ke Docker Hub, sebuah layanan cloud yang memungkinkan kalian untuk membagikan aplikasi Docker kalian ke seluruh dunia. Output dari pekerjaan ini adalah file Docker kalian bisa dilihat secara public pada https://hub.docker.com/r/{Username}/storage-app.

- Berita tagline #YBBA (#YangBiruBiruAja) semakin populer, dan begitu juga sistem yang telah kalian buat untuk membantu Peb dalam rekrutmen pemain. Beberapa klub sepak bola lain mulai menunjukkan minat pada sistem tersebut dan permintaan penggunaan semakin bertambah. Untuk memastikan sistem kalian mampu menangani lonjakan permintaan ini, kalian memutuskan untuk menerapkan skala pada layanan menggunakan Docker Compose dengan instance sebanyak 5. Buat folder terpisah bernama Barcelona dan Napoli dan jalankan Docker Compose di sana.



Catatan: 
Cantumkan file hubdocker.txt yang berisi URL Docker Hub kalian (public).
Perhatikan port  pada masing-masing instance.


# Explanation

**1A**

For 1A, we were tasked to download the datasets and unzip the file. For our implementation, we use system and follow the instruction that were given in the problem, which is ``` kaggle datasets download -d bryanb/fifa-player-stats-database ``` Then, for the unzip, we can use system as well to unzip the file. The full code for 1A can be seen below,

```
#include <stdlib.h>
#include <stdio.h>


int main() {

	
	system("kaggle datasets download -d bryanb/fifa-player-stats-database");
	system("unzip fifa-player-stats-database.zip");

    return 0;
}

```

**1B**

For 1B, we were tasked to show players that has the highest potential while the age is below 25. For the problem, we can use system as well. For the code, it can be seen below,

```
system("sort -t',' -k8nr FIFA23_official_data.csv | awk -F',' 'BEGIN { rank = 1; printf \"%-5s %-20s %-10s %-30s %-20s %-10s %-40s\\n\", \"No\", \"Name\", \"Age\", \"Club\", \"Nationality\", \"Potential\", \"Photo\" } FNR>1 && $3 < 25 && $8 > 85 && $9 != \"Manchester City\" { printf \"%-5s %-20s %-10s %-30s %-20s %-10s %-40s\\n\", rank, $2, $3, $9, $5, $8, $4; rank++ }'");

```
``` sort -t ','``` This code define that there will be sorting which every data is differentiate with a , (coma). 

``` -k8nr FIFA23_official_data.csv ``` This will search the file of FIFA23_official_data.csv within column 8 

``` awk -F',' 'BEGIN { rank = 1; printf \"%-5s %-20s %-10s %-30s %-20s %-10s %-40s\\n\" \"No\", \"Name\", \"Age\", \"Club\", \"Nationality\", \"Potential\", \"Photo\" } ``` This will make a header when the output is printed. 

``` FNR > 1 ``` This will ignore line 1 of the FIFA23_official_data.CSV file. 

``` $3 < 25 && $8 > 85 && $9 != \"Manchester City\" ``` $3 is to find number less than 25 in column 3, $8 will find number bigger than 85 in column 8, and $9 will find club other than Manchester City in column 9. 


**1C**

In this question, we were tasked to create a docker file, image, and container. For the code, it can be seen below,

```
FROM ubuntu:latest
# update package ubuntu
RUN apt-get update -y

```
The above code willl get the latest version of ubuntu and update the version. For the next step, we need to install python and kaggle which the code can be seen below, 

```
#intall python
RUN apt-get install -y unzip python3-pip
# Install kaggle
RUN pip3 install kaggle

```
After successfully install python and kaggle, we need to move the kaggle.json file to the container and change the permission access so that it can download the dataset. Then, we proceed by download and unzip the dataset. 

```
COPY kaggle.json /root/.kaggle/
RUN chmod 600 /root/.kaggle/kaggle.json
RUN kaggle datasets download -d bryanb/fifa-player-stats-database
RUN unzip fifa-player-stats-database.zip -d /

```
Next, we need to install awk, copy, and run the storage.c that we have created before. 

```
RUN apt-get install -y gawk
# Copy storage.c
COPY storage.c /
# Build the executable program
RUN gcc -o storage storage.c

```

Lastly, we need to set an instuction so that it can create a container and run the same output as c file. The code can be seen below, 

```
# Set the command to run the program
CMD ["./storage"]

```

After creating the file, we need to build the docker image as per instruction. 

```
sudo docker build -t eric/soal1 soal1
```

Then, we need to create and run the container. 

```
#create
sudo docker container create --name praktikum eric/soal1

#start
sudo docker start praktikum

```
Lastly, to see the output, we use this following command.

```
docker container logs command

```


**1D**

We push the dockerfile into our repository by login into our docker account and push it. 

```
sudo docker push ericazka/storage-app
```

**1E**

For this question, we need to create a folder named Barcelona and Napoli. Then, we need to execute instance 5 times using docker compose for each folder. The code can be seen below, 

```
version: '3'
services:
  Barcelona:
    build: ./Barcelona
    image: eric/soal1
    deploy:
      replicas: 5

  Napoli:
    build: ./Napoli
    image: eric/soal1
    deploy:
      replicas: 5


```

# Question 2

```
Terdapat file .zip yang berisi folder dan file dari germa. Diminta untuk membuat sebuah FUSE bernama germa.c yang dapat melakukan MKDIR(make folder), RENAME(rename file/directory), RMDIR(remove directory), dan RMFILE(remove file). 

Untuk mengujinya, lakukan :
a. Buatlah folder productMagang pada folder /src_data/germa/products/restricted_list/. Namun, langkah ini akan gagal karena direktori /src_data/germa/products/restricted_list/ belum ada. \

b. Ubah nama folder restricted_list menjadi bypass_list pada folder /src_data/germa/projects/. Ini dapat dilakukan dengan mengganti nama folder restricted_list menjadi bypass_list. Buatlah folder projectMagang di dalam /src_data/germa/projects/bypass_list/. Setelah mengubah nama folder, buatlah folder projectMagang di dalamnya. 

c. Untuk menjaga keamanan, ubah nama folder filePenting di dalam folder projects menjadi restrictedFilePenting. Ini dapat dilakukan dengan mengganti nama folder filePenting menjadi restrictedFilePenting.

d. Hapus semua fileLama di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada folder tersebut agar folder tersebut dapat terhapus

e. Semua kegiatan yang kalian lakukan harus tercatat dalam sebuah file logmucatatsini.txt di luar file .zip yang diberikan kepada Nana, agar pekerjaan yang kamu lakukan dapat dilaporkan kepada Germa. 

```
**2A**

```
// Membuat folder productMagang
    int result = germa_mkdir(productMagang_path, folderMode);
    if (result == 0)
        printf("Folder productMagang berhasil dibuat.\n");
    else 
        printf("Gagal membuat folder productMagang: %s\n", strerror(-result));

// Membuat folder projectMagang
    result = germa_mkdir(projectMagang_path, folderMode);
    if (result == 0)
        printf("Folder projectMagang berhasil dibuat.\n");
    else
        printf("Gagal membuat folder projectMagang: %s\n", strerror(-result));

```
This code is to create productMagang and projectMagang folders.

```
**2B**

```
// Ubah nama folder restricted_list menjadi bypass_list
    result = germa_rename(restrictedList_path, bypassList_path);
    if (result == 0)
        printf("Nama folder restrcited_list berhasil diubah.\n");
    else
        printf("Gagal mengubah nama folder restricted_list: %s\n", strerror(-result));

// Buat folder projectMagang di dalam bypass_list
    result = germa_mkdir(projectMagang_path, folderMode);
    if (result == 0)
        printf("Folder projectMagang berhasil dibuat.\n");

// Hapus folder projectMagang jika sudah ada
        result = germa_rmdir(projectMagang_path);
        if (result == 0)
                printf("Folder projectMagang berhasil dihapus.\n");
        else
                printf("Gagal menghapus folder projectMagang: %s\n", strerror(-result));

```
This code is to rename the restricted_list folder to bypass_list in the /src_data/germa/projects/ folder. And create a projectInternship folder inside /src_data/germa/projects/bypass_list/.                 

```
**2C**

```
// Mengubah nama folder filePenting menjadi restrictedFilePenting
    result = germa_rename(filePenting_path, restrictedFilePenting_path);
    if (result == 0)
        printf("Folder filePenting berhasil diubah menjadi restrictedFilePenting.\n");
    else
        printf("Gagal mengubah nama folder filePenting: %s\n", strerror(-result));

    // Mengubah nama file di dalam restrictedFilePenting
    result = germa_rename(fileProject_path, fileNewProject_path);
    if (result == 0)
        printf("File ourProject.txt berhasil diubah menjadi newProject.txt.\n");
    else
        printf("Gagal mengubah nama file ourProject.txt: %s\n", strerror(-result));

```
This code is to change the name of the FilePenting folder in the projects folder to be RestrictedFilePenting. This can be done by renaming the FilePenting folder to RestrictedFilePenting.

```
**2D**

```
// Menghapus semua "newProject" di dalam folder "restrictedFilePenting"
    char validFiles[MAX_FILES][MAX_FILENAME_LENGTH];
    char invalidFiles[MAX_FILES][MAX_FILENAME_LENGTH];
    int validCount = 0;
    int invalidCount = 0;
    processDirectory(restrictedFilePenting_path, validFiles, &validCount, invalidFiles, &invalidCount);

// Menghapus file "newProject.txt"
    for (int i = 0; i < validCount; i++) {
        if (strcmp(validFiles[i], fileNewProject_path) == 0) {
            germa_unlink(fileNewProject_path);
        }
    }

// Menghapus folder "restrictedFilePenting" dengan kata sihir "bypass"
    germa_unlink(restrictedFilePenting_path);

    return fuse_main(argc, argv, &germa_oper, NULL);
}


```
This code is to delete old files in RestrictedFileLama folder and add unique word "bypass" so that the folder can be deleted.

```

# Question 3

a. Dhafin adalah seorang yang sangat pemalu, tetapi saat ini dia sedang mengagumi seseorang yang dia tidak ingin seorangpun tahu. Maka dari itu, dia melakukan berbagai hal sebagai berikut.
Dhafin ingin membuat sebuah FUSE yang termodifikasi dengan source mount-nya adalah /etc yang bernama secretadmirer.c. 
b. Lalu, untuk menutupi rahasianya, Dhafin ingin semua direktori dan file yang berada pada direktori yang diawali dengan huruf L, U, T, dan H akan ter-encode dengan Base64.  Encode berlaku rekursif (berlaku untuk direktori dan file yang baru di-rename atau pun yang baru dibuat).
c. Untuk membedakan antara file dan direktori, Dhafin ingin semua format penamaan file menggunakan lowercase sedangkan untuk direktori semua harus uppercase. Untuk nama file atau direktori yang hurufnya kurang dari atau sama dengan empat, maka ubah namanya menggunakan data format binary yang didapat dari ASCII code masing-masing karakter.
```
[direktori] helsa -> HELSA
[file] helsa -› helsa
[nama nya kurang dari atau sama dengan 4 huruf] apt -> 01100001 01110000 01110100

```
d. Setiap ingin membuka file, maka harus memasukkan password terlebih dahulu (set password tidak ditentukan a.k.a terserah).
e. Selanjutnya, Dhafin ingin melakukan mounting FUSE tersebut di dalam Docker Container menggunakan Docker Compose (gunakan base image Ubuntu Bionic ). Ketentuannya, terdapat dua buah container sebagai berikut.
Container bernama Dhafin akan melakukan mount FUSE yang telah dimodifikasi tersebut. 
Container bernama Normal akan melakukan mount yang hanya menampilkan /etc yang normal tanpa ada modifikasi apapun. 
f. Kalian masih penasaran siapa yang dikagumi Dhafin? 🥰😍😘😚🤗😻💋💌💘💝💖💗 Coba eksekusi image fhinnn/sisop23 deh, siapa tahu membantu. 🙂

Catatan: 
Pastikan dalam FUSE tersebut (baik yang normal dari /etc maupun yang telah dimodifikasi) dapat melakukan operasi mkdir, cat dan mv.
Pastikan source mount kalian dari .zip yang udah dikasih (inifolderetc/sisop) dan JANGAN PAKAI /etc ASLI KALIAN! kerusakan Linux bukan tanggung jawab kami.


The idea is to create a special filesystem that encrypts files and directories. FUSE is used to connect this program to the operating system, allowing the program to respond to file operations such as reading, writing, creating directories, and so on.

When a file operation is performed by the operating system, FUSE calls the corresponding function in this program, such as ```getattr``` to retrieve file attributes,  ```readdir``` to read directory contents, ```read``` to read file contents, and so on. The program then encrypts or decrypts the data read or written according to the rules provided in the code.

In addition, this program also provides a verifyPassword function that verifies the password before opening a file. If the entered password is correct, the file can be accessed. If the password is incorrect, the file cannot be accessed.

By using this concept, the program creates a filesystem that allows Dhafin to store files with encryption and password verification for accessing those files.

The following is a detailed explanation of the important functions used in this program.

```
#define FUSE_USE_VERSION 28

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <dirent.h>
#include <errno.h>
#include <limits.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
```
we need to have the necessary libraries and headers installed on your system and properly linked during the compilation process.

```
static const char *dirpath = "/Users/tha/Downloads/inifolderetc/sisop";
static const char *password = "mypass";

static const char base64_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

void encodeBase64File(const char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        printf("Failed to open file: %s\n", filename);
        return;
    }

    // Get file size
    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    // Allocate memory for file content
    uint8_t *file_content = (uint8_t *)malloc(file_size);
    if (file_content == NULL) {
        printf("Memory allocation failed\n");
        fclose(file);
        return;
    }

    // Read file content
    size_t bytes_read = fread(file_content, 1, file_size, file);
    fclose(file);

    if (bytes_read != file_size) {
        printf("Failed to read file: %s\n", filename);
        free(file_content);
        return;
    }

    // Encode file content as Base64
    uint8_t input[3], output[4];
    size_t input_len = 0;
    size_t output_len = 0;
    size_t i = 0;
    
    file = fopen(filename, "wb");
    if (file == NULL) {
        printf("Failed to open file for writing: %s\n", filename);
        free(file_content);
        return;
    }

    while (i < file_size) {
        input[input_len++] = file_content[i++];
        if (input_len == 3) {
            output[0] = base64_table[input[0] >> 2];
            output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
            output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
            output[3] = base64_table[input[2] & 0x3F];

            fwrite(output, 1, 4, file);

            input_len = 0;
            output_len += 4;
        }
    }

    if (input_len > 0) {
        for (size_t j = input_len; j < 3; j++) {
            input[j] = 0;
        }

        output[0] = base64_table[input[0] >> 2];
        output[1] = base64_table[((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)];
        output[2] = base64_table[((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)];
        output[3] = base64_table[input[2] & 0x3F];

        fwrite(output, 1, input_len + 1, file);

        for (size_t j = input_len + 1; j < 4; j++) {
            fputc('=', file);
        }

        output_len += 4;
    }

    fclose(file);
    free(file_content);

    printf("File encoded successfully. file: %s\n", filename);
}

bool is_encoded(const char *name) {
    char first_char = name[0];
    if (first_char == 'l' || first_char == 'L' ||
        first_char == 'u' || first_char == 'U' ||
        first_char == 't' || first_char == 'T' ||
        first_char == 'h' || first_char == 'H') {
        return true;
    }
    return false;
}
```
The provided code includes two functions: ```encodeBase64File``` and ```is_encoded```

The ```encodeBase64File``` function is responsible for encoding the contents of a file using Base64 encoding. It starts by opening the file in binary mode, checking if the file was successfully opened, and obtaining the file size. Memory is then allocated to hold the file content, which is read from the file and stored in the allocated memory. The function then proceeds to encode the file content in Base64 format. It iterates over the file content, processing three bytes at a time and converting them into four Base64 characters using bitwise operations and array indexing. The resulting Base64 characters are written to a new file. If the number of input bytes is not a multiple of three, padding characters ('=') are added as necessary. Finally, the function closes the file, frees the allocated memory, and prints a success message indicating that the file has been encoded.

The ```is_encoded``` function takes a name (presumably a filename) as input and determines whether it is encoded based on the first character of the name. If the first character is 'l', 'L', 'u', 'U', 't', 'T', 'h', or 'H', the function returns ```true```, indicating that the name is considered encoded. Otherwise, it returns ```false```.

These functions appear to be part of a larger program or codebase related to file encoding and decoding using Base64. The ```encodeBase64File``` function provides the capability to encode file contents in Base64 format, while the ```is_encoded``` function helps identify whether a given filename is encoded or not.

```
static int xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath, "%s%s", dirpath, path);

    res = lstat(fpath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}
```
The ```xmp_getattr``` function constructs the full path by concatenating the base directory path (```dirpath```) with the provided ```path```, and then calls ```lstat``` to retrieve the attributes of the specified file or directory. It handles any potential errors by returning the appropriate error code to the caller.

```
void modify_filename(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = tolower(name[i]);
    }
}

void modify_directoryname(char *name)
{
    for (int i = 0; name[i]; i++)
    {
        name[i] = toupper(name[i]);
    }
}
```
The ```modify_filename``` this function takes a character array `name` as input, which typically represents a `filename`. It iterates over each character in the `name array` using a `for loop`. For each character, it applies the `tolower` function to convert it to lowercase. By doing so, the function modifies the characters in name to be in lowercase.

The `modify_directoryname` This function takes a character array `name` as input, which typically represents a `directory name`.
It iterates over each character in the name array using a for loop.For each character, it applies the `toupper` function to convert it to uppercase.By doing so, the function modifies the characters in name to be in uppercase.
```
void convert_to_binary(char *name)
{
    int len = strlen(name);
    char *temp = (char *)malloc((8 * len + len) * sizeof(char)); // Allocate memory for modified string

    int index = 0; // Index for the modified string

    for (int i = 0; i < len; i++)
    {
        char ch = name[i];

        // Convert character to binary string
        for (int j = 7; j >= 0; j--)
        {
            temp[index++] = (ch & 1) + '0'; // Convert bit to character '0' or '1'
            ch >>= 1;                       // Shift right by 1 bit
        }

        if (i != len - 1)
        {
            temp[index++] = ' '; // Add space separator between binary codes
        }
    }

    temp[index] = '\0'; // Null-terminate the modified string

    strcpy(name, temp); // Update the original string with the modified string
    free(temp);         // Free the allocated memory
}
```
The `convert_to_binary` function converts each character in the input string into its binary representation, separating each binary code by a space. The original string is modified with the binary representation, allowing the binary-coded string to be used further.
```
void changePath(char *str1, char *str2) {
    char *slash = strrchr(str1, '/');
    char temp[100];
    strcpy(temp, str1);
    if (slash != NULL) {
        ++slash; // Move past the slash
        strcpy(slash, str2);
        rename(temp, str1);
    }
}
```
The `changePath` function modifies a file path by replacing the filename component with a new filename. It first locates the last occurrence of the '/' character in the original path, and if found, it replaces the filename with the new filename. The function then `renames` the file or directory using the rename function to reflect the modified path.
```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;

    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;

    dp = opendir(fpath);

    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        char name[100];
        strcpy(name, de->d_name);
        char cur_path[1500];
        sprintf(cur_path, "%s/%s", fpath, de->d_name);

        res = (filler(buf, name, &st, 0));

        if (res != 0)
            break;
        if(de->d_type == DT_REG) {
            if(is_encoded(name)) {
                encodeBase64File(cur_path);
            }
            else if(strlen(name) <= 4) {
                convert_to_binary(name);
            }
            else {
                modify_filename(name);
            }
            changePath(cur_path, name);
        }
        else {
            if(strlen(name) <= 4) {
                convert_to_binary(name);
            }
            else {
                modify_directoryname(name);
            }
            changePath(cur_path, name);
            xmp_readdir(cur_path, buf, filler, offset, fi);
        }
    }

    closedir(dp);

    return 0;
}
```
The `xmp_readdir` function provided in the code snippet is responsible for reading the contents of a directory specified by the `path` parameter. It iterates over each directory entry using `readdir` and obtains the file information for each entry. The function then performs different operations based on whether the entry is a regular file or a directory. For regular files, it can modify the filename, encode it using `encodeBase64File`, convert it to binary using `convert_to_binary`, or leave it unchanged. The function renames the file with the modified name using `changePath`. For directories, it can modify the directory name, convert it to binary, or leave it unchanged. The function renames the directory with the modified name using changePath and recursively calls `xmp_readdir` to read the contents of the subdirectory. Finally, the function closes the directory and returns 0 to indicate success.
```
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    char fpath[1000];
    if (strcmp(path, "/") == 0) {
        path = dirpath;
        sprintf(fpath, "%s", path);
    } else
        sprintf(fpath, "%s%s", dirpath, path);

    int res = 0;
    int fd = 0;

    (void)fi;

    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);

    return res;
}
```
The `xmp_read` function provided in the code snippet is responsible for reading the contents of a file specified by the path parameter. It constructs the full path to the file based on the provided `path` and the `dirpath`. It opens the file in read-only mode using  `open`, reads the requested number of bytes from the specified offset into the buffer using `pread`, and returns the number of bytes read or an error code. Finally, it closes the file using `close`. The function ensures that the file operations are performed correctly and handles error conditions by returning the appropriate error codes.

```
static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    (void)fi;

    if (strcmp(path, "/") == 0) {
        path = dirpath;
    } else {
        char fpath[1000];
        sprintf(fpath, "%s%s", dirpath, path);
        if (access(fpath, F_OK) == 0) {
            char input_password[100];
            printf("Masukkan password: ");
            fflush(stdout);
            fgets(input_password, sizeof(input_password), stdin);

            size_t len = strlen(input_password);
            if (len > 0 && input_password[len - 1] == '\n')
                input_password[len - 1] = '\0';

            while (strcmp(input_password, password) != 0) {
                printf("Maaf, password salah! Silahkan coba kembali.\nMasukkan password: ");
                fflush(stdout);
                fgets(input_password, sizeof(input_password), stdin);

                len = strlen(input_password);
                if (len > 0 && input_password[len - 1] == '\n')
                    input_password[len - 1] = '\0';
            }
        }
    }

    return 0;
}
```
The `xmp_open` function provided in the code snippet handles file opening operations. It checks if the file path corresponds to the root directory or a specific file. For non-root directory paths, it constructs the full path and checks if the file exists. If the file exists, it prompts the user to enter a password for validation. The function compares the input password with the predefined password, repeating the process until a correct password is entered. The function ensures that access to the file is granted only after the correct password is provided.
```
static int xmp_mkdir(const char *path, mode_t mode)
{
    char fpath[1000];
    sprintf(fpath, "%s%s", dirpath, path);

    // Modify the directory name to all uppercase
    char modified_name[256];
    strcpy(modified_name, path);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    // Create the directory with the modified name
    int res = mkdir(fpath, mode);
    if (res == -1) {
        return -errno;
    }

    return 0;
}
```
The `xmp_mkdir` function provided in the code snippet is responsible for creating a new directory with a modified name. The function takes the `path` parameter, which represents the desired directory path relative to the `dirpath` variable (representing the base directory path). It constructs the full path by concatenating dirpath and path using `sprintf`.

Next, the function modifies the directory name by creating a separate character array named `modified_name` and copying the original `path` into it. It then iterates over each character in `modified_name` and converts it to uppercase using the `toupper` function. This ensures that the new directory name will be in uppercase.

After modifying the directory name, the function calls the `mkdir` function with the `fpath` (full path) and `mode` parameters to create the directory with the modified name. If the directory creation fails, the function returns the corresponding error code using `-errno`.

Finally, the function returns 0 to indicate success if the directory creation was successful. In summary, the `xmp_mkdir` function creates a new directory with a modified name by modifying the original directory name to be in uppercase and using the `mkdir` function to create the directory with the modified name.
```
static int xmp_rename(const char *from, const char *to)
{
    char fpath_from[1000];
    char fpath_to[1000];
    sprintf(fpath_from, "%s%s", dirpath, from);
    sprintf(fpath_to, "%s%s", dirpath, to);

    // Modify the destination name to all uppercase
    char modified_name[256];
    strcpy(modified_name, to);
    for (int i = 0; modified_name[i] != '\0'; i++) {
        modified_name[i] = toupper(modified_name[i]);
    }

    // Rename the file/directory with the modified name
    int res = rename(fpath_from, fpath_to);
    if (res == -1) {
        return -errno;
    }

    return 0;
}
```
The `xmp_rename` function provided in the code snippet handles the renaming of files or directories. It constructs the full source and destination paths by concatenating the `dirpath` and the provided from and to paths using `sprintf`. The destination name is then modified by converting it to uppercase.

The function calls the `rename` function with the source and destination paths to perform the renaming operation. If the renaming fails, it returns the corresponding error code using `-errno`.

Overall, the `xmp_rename` function ensures the proper renaming of files or directories by modifying the destination name to be in uppercase and using the rename function. It handles any errors that may occur during the renaming process.
```
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .open = xmp_open,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
};
```
The purpose of this code is to define a `struct fuse_operations` variable named `xmp_oper` that specifies the functions to be called for different file system operations. These functions are implemented elsewhere in the code, and by assigning them to the respective members of `xmp_oper`, they are associated with the corresponding FUSE operations. This variable is then used in the main FUSE initialization to provide the implementation for the specified file system operations.
```
int main(int argc, char *argv[])
{
    umask(0);

    // Run the FUSE filesystem
    int fuse_stat = fuse_main(argc, argv, &xmp_oper, NULL);

    return fuse_stat;
}
```
The `main` function sets the file mode creation mask to 0, runs the FUSE filesystem using `fuse_main` with the appropriate arguments, and returns the resulting status code. It serves as the starting point for the program and handles the execution of the FUSE filesystem.


